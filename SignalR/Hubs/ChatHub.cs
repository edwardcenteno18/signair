﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalR.Hubs
{
    public class ChatHub : Hub
    {
        public void Enviar(string nombre, string mensaje)
        {
            Clients.All.addNuevoMensajeEnPagina(nombre, mensaje);
        }

        public static int contador = 0;

        public override Task OnConnected()
        {
            contador++;
            Clients.All.mostrarConectados(contador);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {

            contador--;
            Clients.All.mostrarConectados(contador);
            return base.OnDisconnected(stopCalled);
        }
    }
}